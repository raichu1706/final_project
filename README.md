# 데이터 기반 영화 추천 웹 서비스
## 0. 개발 환경
- Python 3.10.8
- Django 3.2.13
- Vue 2.6.14
- Vuex 3.6.2

```
  final-pjt-front

  npm i
  npm run serve
```

```
  fianl-pjt-back

  python -m venv venv
  source venv/Scripts/acitvate
  pip install -r requirements.txt
  python manage.py makemigrations
  python manage.py migrate
  python manage.py loaddata movies.json
  python manage.py runserver
```

## 1. 팀원 정보 및 업무 분담 내역
    팀장: 문영식

    역할: UI 구성 및 CSS 스타일 적용

    팀원: 윤호중

    역할: Django Rest API 설계, 추천 알고리즘, 프론트엔드(Vue.js)

## 2. 목표 서비스 구현 및 실제 구현 정도
    우리가 실제 구현한 것들

    - 토큰 기반 인증(회원가입, 로그인, 로그아웃)
    - 검색
    - 추천
    - 전체 영화 조회
    - 세부 영화 조회
    - 프로필 페이지


    다음에 구현해야할 부분
    - JWT 이해하고 적용하기
    - 인증 보안 강화하기(현재는 브라우저 로컬 스토리지에 토큰 값을 저장하고 있음)
    - 현재는 DB에서 한번에 모든 데이터를 가져와 브라우저 로컬스토리지에 저장하는 방식이기 때문에 렌더링 속도가 느리다.(굉장히 크리티컬한 문제), 현재 브라우저 화면에서 필요한 데이터만 불러오고 스크롤을 내리면 추가적으로 데이터를 불러오는 방식을 적용하면 이 문제를 해결할 수 있을 것 같다
    - 추천 기능 고도화
    - 현재 상영 중인 영화 클릭시 해당 영화 예매 사이트로 이동하기 기능 추가
    - 유저가 영화들을 선별해서 만든 콜렉션 관련 기능 추가
  
## 3. 데이터베이스 모델링

![데이터베이스 모델링(ERD)](/image/ssafy.png)

## 4. 영화 추천 알고리즘
    우리 서비스에서는 2가지의 추천 알고리즘을 적용했다. 모두 메모리기반의 통계적인 추천 알고리즘으로, 구체적으로 다음과 같다.

```
1. User-based Collaborative Filtering

해당 알고리즘을 구현하기 위해, 먼저 TMDB에서 약 3000개의 영화에 대한 15만 건의 유저의 리뷰 데이터를 수집한 후, 해당 데이터를 가지고 User-Item matrix를 생성한 후, 이를 서버에 저장,
유저가 우리 서비스를 하면서 리뷰한 영화 정보를 바탕으로 서버에 저장된 다른 유저의 평점 데이터와 코사인 유사도를 계산한 후, 상위 유저가 높게 평가한 영화를 추천해준다. 이때, 처음에 유저가 리뷰를 하나도 하지 않은 경우에는 랜덤으로 추천해준다. 이 알고리즘은 우리 서비스에서 적용한 다른 추천 알고리즘인 Contents-based Recommendation 알고리즘과 달리, 유저가 우리 서비스를 이용하면서 리뷰르 계속 진행할수록 추천 결과가 달라질 수 있다.
```

```
2. Contents-based Recommendation

해당 추천 알고리즘에서는 영화의 내용에 대한 핵심적인 정보를 가지고 있는 줄거리에 집중을 해서 
db에 저장된 약 3000개의 영화의 줄거리 데이터에서 형태소 분석을 실시한 후, 핵심 키워드들을 선택한 후, TF-IDF를 계산하였다.
예를 들어 '철수가 밥을 먹는다' 라는 문장이 있으면 '철수'와 '밥'이라는 키워드로부터 문장의 의미를 대략 유추할 수 있는데, 해당 아이디어를 바탕으로 영화 데이터에서 피처를 추출하였다.
이후 해당 결과를 서버에 저장한 후, 영화간 코사인 유사도를 계산해, 비슷한 내용의 영화를 추천하게 된다.

[코드]
import requests
import json
import pandas as pd
import numpy as np
import re
from konlpy.tag import Mecab
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import defaultdict

removal_list =  "‘, ’, ◇, ‘, ”,  ’, ', ·, \“, ·, △, ●,  , ■, (, ), \", >>, `, /, -,∼,=,ㆍ<,>, .,?, !,【,】, …, ◆,%"

def cleansing_numbers(sentence):
    # 숫자를 전처리(delexicalization) 하는 함수    
    sentence = re.sub('[0-9]+', 'NUM', sentence)
    sentence = re.sub('NUM\s+', "NUM", sentence)
    sentence = re.sub('[NUM]+', "NUM", sentence)
    
    return sentence

def cleansing_special(sentence):
    # 특수문자를 전처리를 하는 함수
    sentence = re.sub("[.,\'\"’‘”“!?]", "", sentence)
    sentence = re.sub("[^가-힣0-9a-zA-Z\\s]", " ", sentence)
    sentence = re.sub("\s+", " ", sentence)
    
    sentence = sentence.translate(str.maketrans(removal_list, ' '*len(removal_list)))
    sentence = sentence.strip()
    
    return sentence
  
def preprocessing_overview(sentence):
    mecab = Mecab()
    cleansed = cleansing_numbers(sentence)
    cleansed = cleansing_special(cleansed)
    cleansed_overview = ' '.join(set(mecab.nouns(cleansed)))
    return cleansed_overview

cleansed_overviews = []
for i in range(movies_df.shape[0]):
    cleansed_overviews.append(preprocessing_overview(movies_df.loc[i]['overview']))

tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(cleansed_overviews)

word2id = defaultdict(lambda : 0)
for idx, feature in enumerate(tfidf_vectorizer.get_feature_names()):
    word2id[feature] = idx
for idx, sentence in enumerate(cleansed_overviews):
    results = [(token, tfidf_matrix[idx, word2id[token]]) for token in sentence.split()]
    results.sort(key = lambda element : element[1], reverse=True)
    print(results)
    print("\n")

movie_representation = pd.DataFrame(columns=list(tfidf_vectorizer.get_feature_names()))
n_col = movie_representation.shape[1]
for idx, sentence in enumerate(cleansed_overviews):
    movie_representation.loc[idx] = [0] * n_col
    for token in sentence.split(' '):
        movie_representation.loc[idx][token] = tfidf_matrix[idx, word2id[token]]

from sklearn.metrics.pairwise import cosine_similarity

def cos_sim_matrix(a, b):
    cos_sim = cosine_similarity(a, b)
    result_df = pd.DataFrame(data=cos_sim, index=[a.index])

    return result_df

cs_df = cos_sim_matrix(movie_representation, movie_representation)
cs_df.head()
```

## 5. 서비스 대표 기능
우리 서비스에서 추천 기능 이외에 제공하는 기능은 다음과 같습니다.
- 커뮤니티 서비스를 위한 영화 상세페이지 댓글 및 대댓글 기능 
[!'영화 상세페이지'](./image/detail.png)



- 커뮤니티 서비스를 위한 프로필 페이지 및 팔로우/팔로잉 기능
[!'팔로우/팔로잉 기능'](./image/review.png)



- 영화 검색 기능 + 장르 기반 필터링
[!'영화 검색 기능'](./image/search.png)


- 유저의 나이 정보를 바탕으로 성인이 아닌 경우, 성인 영화 필터링 기능
- 추천 알고리즘을 위한 영화 리뷰 기능(평점)


## 6. 느낀점
```
윤호중

이번 프로젝트를 통해 싸피에서 1학기 동안 배운 웹 관련 내용들을 모두 적용해볼수 있었습니다. 특히 추천 알고리즘을 적용하면서, 머신러닝, 딥러닝을 실제 서비스에 적용하기 위해서는 서비스에서 적절한 데이터 파이프라인의 필요성 등, 예전에 AI 모델링만 했을 때는, 고민하지 못했던 부분들에 대해 느꼈습니다. 또한 처음에 전체적인 설계에 대해 충분히 고민하지 않고, 그냥 하면서 필요한 기능들을 바로 구현하는 방식으로 프로젝트를 진행했는데, 뭔가 코드가 지저분해진 것 같습니다. 싸피에서 뭔가 코드에 대해 리뷰해주고, 리팩토링 해볼수 있는 시간이 있으면 좋을것 같고, 위에서 설명한 것 처럼 원래 구현하고 싶었던 서비스와 비교해서 많은 부분을 구현하지 못하고, 성능 최적화도 하지 못했는데 그런 부분을 배웠고, 또 아쉬움이 남습니다. 마지막으로 2인 1조로 페어프로그래밍을 진행했는데, 개발에서 페어 프로그래밍을 이번에 처음 시도해봤는데, 서로가 가진 능력을 100%로 끌어올리지 못해서 아쉽습니다. 고생하셨습니다.
```

```
문영식

싸피 1학기의 마무리하는 시점에 배웠던 것을 다시 한번 복습하면서 공부하게 된 순간이었던 것 같습니다. 하지만 그 와 동시에 싸피에서 배운 것을 100% 이용하지 못한 것에 내가 아직 배워야하는 것이 많구나 느낀 10일간의 프로젝트였던 것 같습니다. 이번 페어 프로젝트의 특성은 단순하게 파일을 작성하는 것이 아닌 개발자의 길이 어떤 것인지에 대해서 알게 되었던 것 같습니다. UI의 구성이나 알고리즘을 통해서 고객에게 전달하는 것부터 시작하여, 동료 개발자들과의 협업이 얼마나 중요한지까지 수업을 배우기보다는 사회를 배우고 가는 기분으로 이번 프로젝트를 진행했던 것 같습니다. 이번에 페어 프로그램을 처음 해 보았는데 힘든과정에 성과보다는 아쉬움이 많이 남았던 것 같습니다. 이러한 이쉬움점들을 오늘의 이 기억을 디딤돌로 삼아서 앞으로 개발자로서 성장하고, 다음 프로젝트들을 진행하는데 잘 참고할 수 있도록 할 수 있도록 하겠습니다. 한 학기간 수고 많으셨습니다.

```
