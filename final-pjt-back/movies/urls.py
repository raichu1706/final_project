from django.urls import path
from . import views

urlpatterns = [
    path('movies/', views.MoviesView.as_view()),
    path('movies/<int:movie_id>/', views.MovieView.as_view()),
    path('movies/<int:movie_id>/comments/', views.CommentsView.as_view()),
    path('movies/<int:movie_id>/users/<int:user_id>/', views.review),
    path('review/movies/<int:movie_id>/users/<int:user_id>/', views.getReview),
    path('comments/<int:comment_id>/', views.CommentView.as_view()),
    path('currentuser/', views.getCurrentUser),
    path('users/', views.getUsers),
    path('users/<int:user_id>/', views.getUser),
    path('like/comments/<int:comment_id>/<int:user_id>/', views.comment_like),
    path('unlike/comments/<int:comment_id>/<int:user_id>/', views.comment_unlike),
    path('movies/search/', views.getSearch),
    path('comments/<int:parent_comment>/child/', views.get_child_comments),
    path('recommendations/userbase/', views.get_user_based_recommendations),
    path('recommendations/contentbase/<int:target_id>/', views.get_contents_based_recommendations),
    path('movies/<int:user_id>/reviews/', views.get_movie_user_review),
    path('movies/<int:user_id>/followings/reviews/', views.get_movie_followings_review)
]