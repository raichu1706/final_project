from django.db import models
from django.conf import settings

# Create your models here.
class Movie(models.Model):
    movie_id = models.PositiveIntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    release_date = models.DateField()
    runtime = models.PositiveIntegerField()
    overview = models.TextField(null=True)
    poster_path = models.TextField(null=True)
    backdrop_path = models.TextField(null=True)
    video_key = models.TextField(null=True)
    adult = models.BooleanField()
    user = models.ManyToManyField(settings.AUTH_USER_MODEL, through='Movie_User', null=True)
    genres = models.ManyToManyField('Genre')


class Movie_User(models.Model):
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    score = models.PositiveIntegerField()


class Genre(models.Model):
    genre_id = models.PositiveIntegerField(primary_key=True)
    genre_name = models.CharField(max_length=100)


class Comment(models.Model):
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    like_users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='like_comments', blank=True)
    unlike_users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='unlike_comments', blank=True)
    parent_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
