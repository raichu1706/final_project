from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import Movie, Movie_User, Genre, Comment


class MoviesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Movie
        fields = ['movie_id', 'title', 'poster_path']


class MovieSerializer(serializers.ModelSerializer):
    
    genre_names = serializers.SerializerMethodField('get_genre_name')

    class Meta:
        model = Movie
        fields = ['movie_id', 'title', 'release_date', 'runtime', 'overview', 'poster_path', 'backdrop_path', 'video_key', 'adult', 'user', 'genre_names']

    def get_genre_name(self, obj):
        genre_names = [value[1] for value in obj.genres.values_list()]
        return genre_names


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'


class CommentCreateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ['movie', 'user', 'created_at', 'updated_at', 'parent_comment']


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'is_superuser', 'is_staff', 'nickname', 'birthday', 'sex']


class MovieUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie_User 
        fields = '__all__'
        read_only_fields = ['movie', 'user']