from datetime import datetime
import re
import numpy as np
import pandas as pd
import os
import random
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import jaccard_score
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST
from .serializers import MoviesSerializer, MovieSerializer, CommentSerializer, CommentCreateSerializer, UserSerializer, MovieUserSerializer
from .models import Movie, Movie_User, Comment, Genre
from accounts.models import Profile
from .permissions import CustomReadOnly


# Create your views here.
class MoviesView(APIView):
    
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        movies = Movie.objects.all()
        serializer = MoviesSerializer(movies, many=True)
        return Response(serializer.data, status=HTTP_200_OK)


class MovieView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, movie_id):
        movie = get_object_or_404(Movie, movie_id=movie_id)
        serializer = MovieSerializer(movie)
        # print(Movie_User.objects.filter(movie=movie.movie_id))
        return Response(serializer.data, status=HTTP_200_OK)


class CommentsView(APIView):

    permission_classes = (CustomReadOnly,)

    def get(self, request, movie_id):
        movie = Movie.objects.get(movie_id=movie_id)
        comments = Comment.objects.filter(movie=movie)
        parent_comments = []
        for comment in comments:
            if not comment.parent_comment_id:
                parent_comments.append(comment)
        serializer = CommentSerializer(parent_comments, many=True)
        data = serializer.data
        for comment in data:
            comment['nickname'] = get_user_model().objects.get(pk=comment['user']).nickname
            comment['profile_image'] = Profile.objects.get(user=comment['user']).image
            comment['profile_image'] = str(comment['profile_image']).split('/')[-1]
            comment['profile_image'] = f"http://127.0.0.1:8000/media/profile/{comment['profile_image']}/"
            comment['n_childs'] = Comment.objects.filter(parent_comment=comment['id']).count()

        return Response(data, status=HTTP_200_OK)
    
    def post(self, request, movie_id):
        movie = Movie.objects.get(movie_id=movie_id)
        serializer = CommentCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            if request.data.get('parent'):
                parent = Comment.objects.get(pk=request.data.get('parent'))
                serializer.save(movie=movie, user=request.user, parent_comment=parent)
            else:
                serializer.save(movie=movie, user=request.user)
            data = dict(serializer.data)
            data['nickname']=get_user_model().objects.get(pk=data['user']).nickname
            return Response(data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class CommentView(APIView):
    
    permission_classes = (CustomReadOnly,)

    def put(self, request, comment_id, format=None):
        # movie = Movie.objects.get(movie_id=movie_id)
        comment = get_object_or_404(Comment, pk=comment_id)
        serializer = CommentCreateSerializer(comment, data=request.data)
        if serializer.is_valid(raise_exception=True):
            # serializer.save(movie=movie, user=request.user)
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, request, comment_id, format=None):
        comment = get_object_or_404(Comment, pk=comment_id)
        comment.delete()
        return Response(status=HTTP_204_NO_CONTENT)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getUsers(request):
    queryset = get_user_model().objects.all()
    serializer = UserSerializer(queryset, many=True)
    return Response(serializer.data, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getUser(request, user_id):
    queryset = get_user_model()
    user = get_object_or_404(queryset, pk=user_id)
    serializer = UserSerializer(user)
    return Response(serializer.data, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getCurrentUser(request):
    data = {
        'user_id': request.user.id,
        'user_nickname': request.user.nickname,
        'user_birthday': request.user.birthday,
        'user_sex': request.user.sex,
    }
    return Response(data, status=HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def review(request, movie_id, user_id):
    reviewItem = Movie_User.objects.filter(movie_id=movie_id, user_id=user_id)
    if reviewItem:
        serializer = MovieUserSerializer(reviewItem, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response(status=HTTP_201_CREATED)
    else:
        serializer = MovieUserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(movie=Movie.objects.get(pk=movie_id), user=get_user_model().objects.get(pk=user_id))
        return Response(status=HTTP_201_CREATED)
    


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getReview(request, movie_id, user_id):
    reviewItem = Movie_User.objects.filter(movie_id=movie_id, user_id=user_id)
    if reviewItem:
        return Response({'score': reviewItem.values()[0]['score']}, status=HTTP_200_OK)
    else:
        return Response(None)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def comment_like(request, comment_id, user_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    user = get_object_or_404(get_user_model(), pk=user_id)
    if user.like_comments.filter(pk=comment_id).exists():
        user.like_comments.remove(comment.pk)
        is_like = False
    else:
        user.like_comments.add(comment.pk)
        is_like = True
    return Response(is_like)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def comment_unlike(request, comment_id, user_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    user = get_object_or_404(get_user_model(), pk=user_id)
    if user.unlike_comments.filter(pk=comment_id).exists():
        user.unlike_comments.remove(comment.pk)
        is_unlike = False
    else:
        user.unlike_comments.add(comment.pk)
        is_unlike = True
    return Response(is_unlike)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def getSearch(request):
    search_keyword = request.query_params.get('keyword')
    search_genres = request.query_params.get('genres')
    now = datetime.now()
    is_adult_user = True if int(str(now)[:4]) - int(request.query_params.get('birthday')[:4]) >= 20 else False
    if not is_adult_user:
        movies = Movie.objects.filter(adult=False)
    else:
        movies = Movie.objects.all()
    
    if search_genres:
        search_genres = search_genres.rstrip().split(' ')
        filtered_movies = []
        for movie in movies:
            movie_genres = [str(genre['genre_id']) for genre in movie.genres.values()]
            if check_genre(search_genres, movie_genres):
                filtered_movies.append(movie)
        
        if search_keyword:
            results = []
            for movie in filtered_movies:
                if search_keyword in movie.title:
                    results.append(movie)
        
        else:
            results = filtered_movies
    
    else:
        if search_keyword:
            results = []
            for movie in movies:
                if search_keyword in movie.title:
                    results.append(movie)
        
        else:
            results = movies
    
    serializer = MoviesSerializer(results, many=True)
    return Response(serializer.data, status=HTTP_200_OK)


def check_genre(search_genres, movie_genres):
    for genre in movie_genres:
        if genre in search_genres:
            return True
    return False


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_child_comments(request, parent_comment):
    comments = Comment.objects.filter(parent_comment=parent_comment)
    serializer = CommentSerializer(comments, many=True)
    data = serializer.data
    for item in data:
        item['nickname'] = get_user_model().objects.get(pk=item['user']).nickname
        item['profile_image'] = Profile.objects.get(user=item['user']).image
        item['profile_image'] = str(item['profile_image']).split('/')[-1]
        item['profile_image'] = f"http://127.0.0.1:8000/media/profile/{item['profile_image']}/"

    return Response(data, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user_based_recommendations(request):
    params = dict(request.query_params)

    data = {
        'user_id': params['data[user_id]'][0],
        'user_nickname': params['data[user_nickname]'][0],
        'user_birthday': params['data[user_birthday]'][0],
        'user_sex': params['data[user_sex]'][0]
    }
    
    ratings_df = pd.read_csv(f'{os.getcwd()}/movies/data/ratings.csv')
    sparse_matrix = ratings_df.groupby('tmdb_id').apply(lambda x: pd.Series(x['rating'].values, index=x['userId'])).unstack()
    sparse_matrix.index.name = 'movieId'
    user_sparse_matrix = sparse_matrix.fillna(0).transpose()
    movie_ids = list(user_sparse_matrix.columns)
    n_movies = len(movie_ids)
    enc_table = {
        movie_ids[i]: i for i in range(n_movies)
    }
   
    score_objects = list(Movie_User.objects.filter(user=data['user_id']).values())
    ratings = np.array([0 for _ in range(n_movies)])
    users_already_rated = set()
    for score_object in score_objects:
        if enc_table.get(score_object['movie_id']):
            ratings[enc_table[score_object['movie_id']]] = score_object['score']
            users_already_rated.add(score_object['movie_id'])
  
    movies = []
    if sum(ratings):
        ratings = ratings.reshape(1, -1)
        sim_df = pd.DataFrame(index=user_sparse_matrix.index)
        sim_df['similarity'] = cosine_similarity(user_sparse_matrix.values, ratings).reshape(-1)
        sim_df.sort_values('similarity', ascending=False, inplace=True)
        movies_set = set()
        for userId in sim_df.index:
            filtered_df = ratings_df[ratings_df['userId'] == userId][['tmdb_id', 'rating']]
            filtered_df = filtered_df[filtered_df['rating'] > 3.5]['tmdb_id']
            flag = False
            for item in filtered_df.values:
                if item not in movies_set and item not in users_already_rated:
                    movies_set.add(item)
                    movies.append(Movie.objects.get(pk=item))
                    if len(movies) >= 10:
                        flag = True
                        break
            if flag:
                break

    if len(movies) < 10:
        random_selected_movie_ids = random.sample(movie_ids, 10)
        movies = [Movie.objects.get(pk=movie_id) for movie_id in random_selected_movie_ids]
        
    serializer = MoviesSerializer(movies, many=True)
    
    res = {
            'recommendations': serializer.data
        }
    
    return Response(res, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_contents_based_recommendations(request, target_id):
    contents_similarity_df = pd.read_csv(f'{os.getcwd()}/movies/data/contents_similarity.csv', index_col=0)
    movie_ids = contents_similarity_df.columns
    content_similarity = [(contents_similarity_df.loc[target_id, movie_id], movie_id) for movie_id in movie_ids if movie_id != str(target_id)]
    content_similarity.sort(key=lambda x: -x[0])
    movies = [Movie.objects.get(pk=reference_id) for _, reference_id in content_similarity[:5]]
    serializer = MoviesSerializer(movies, many=True)
    return Response(serializer.data, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_movie_user_review(request, user_id):
    reviews = Movie_User.objects.filter(user=user_id)
    movies = []
    scores = []
    for review in reviews.values():
        movies.append(Movie.objects.get(pk=review['movie_id']))
        scores.append(review['score'])
    data = {
        'movies': MoviesSerializer(movies, many=True).data,
        'scores': scores
    }

    return Response(data, status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_movie_followings_review(request, user_id):
    user = Profile.objects.get(pk=user_id)
    movies = []
    scores = []
    for following in user.followings.all():
        reviews = Movie_User.objects.filter(user=following.pk)
        for review in reviews.values():
            movies.append(Movie.objects.get(pk=review['movie_id']))
            scores.append(review['score'])
    data = {
        'movies': MoviesSerializer(movies, many=True).data,
        'scores': scores
    }

    return Response(data, status=HTTP_200_OK)
