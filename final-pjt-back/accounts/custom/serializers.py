from django.db import transaction
from rest_framework import serializers
from dj_rest_auth.registration import serializers as rest_auth_serializers
import re


class CustomRegisterSerializer(rest_auth_serializers.RegisterSerializer):
    nickname = serializers.CharField(
        max_length=100,
    )
    birthday = serializers.CharField(
        max_length=10,
    )
    sex = serializers.CharField(
        max_length=1,
    )

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(_("The two password fields didn't match."))
        regex = r'\d{4}-\d{2}-\d{2}'
        if not bool(re.match(regex, data['birthday'])):
            raise serializers.ValidationError(_("birthday fields didn't match."))
        if data['sex'] not in ['M', 'F']:
            raise serializers.ValidationError(_("sex fields didn't match."))
        return data
        
    # Define transaction.atomic to rollback the save operation in case of error
    @transaction.atomic
    def save(self, request):
        user = super().save(request)
        user.nickname = self.data.get('nickname')
        user.birthday = self.data.get('birthday')
        user.sex = self.data.get('sex')
        user.save()
        return user