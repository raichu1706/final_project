from django.urls import path
from .views import ProfileView, ProfileUpdateView, follow


urlpatterns = [
  path('profile/<int:pk>/', ProfileView.as_view()),
  path('profile/<int:pk>/upload/', ProfileUpdateView.as_view()),
  path('follow/<int:my_pk>/<int:user_pk>/', follow)
]