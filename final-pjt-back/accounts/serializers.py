from .models import Profile
from rest_framework import serializers


class ProfileSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Profile
        fields = ['profileImage_url', 'followers', 'followings']


class ProfileUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = '__all__'
        read_only_fields = ['user', 'followers', 'followings']
