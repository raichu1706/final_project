from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class UserManager(BaseUserManager):

    def create_user(self, email, password, nickname, birthday, sex, **kwargs):
        if not email:
            raise ValueError('Users must have an email address')
        
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            nickname=nickname,
            birthday=birthday,
            sex=sex,
        )
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email=None, password=None, nickname=None, birthday=None, sex=None, **extra_fields):
        superuser = self.create_user(
            email=email,
            password=password,
            nickname=nickname,
            birthday=birthday,
            sex=sex,
        )
        superuser.is_staff = True
        superuser.is_superuser = True
        superuser.is_active = True
        superuser.save(using=self._db)
        return superuser

class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=30, unique=True, null=False, blank=False)
    nickname = models.CharField(max_length=100, unique=True, null=False, blank=False)
    birthday = models.CharField(max_length=10)
    sex = models.CharField(max_length=1, choices=[('M', 'Male'), ('F', 'Female')])
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nickname', 'birthday', 'sex']


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, primary_key=True)
    image = models.ImageField(upload_to='profile', default='default.png')
    followers = models.ManyToManyField('self', symmetrical=False, related_name='followings')

    @property
    def profileImage_url(self):
        return self.image.url
        

@receiver(post_save, sender=get_user_model())
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
