import Vue from 'vue'
import VueRouter from 'vue-router'
import LogIn from '@/components/LogIn.vue'
import SignUp from  '@/components/SignUp.vue'
import LogOut from '@/components/LogOut.vue'
import MainView from '@/views/MainView.vue'
import DetailView from '@/views/DetailView.vue'
import HomeView from '@/views/HomeView.vue'
import PageNotFound from '@/views/PageNotFound.vue'
import ProfileView from '@/views/ProfileView.vue'
import SearchView from '@/views/SearchView.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LogIn
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUp
  },
  {
    path: '/movies',
    name: 'movies',
    component: MainView
  },
  {
    path: '/movies/:movie_id',
    name: 'movie',
    component: DetailView
  },
  {
    path: '/logout',
    name: 'logout',
    component: LogOut
  },
  {
    path: '/profile/:user_id',
    name: 'profile',
    component: ProfileView
  },
  {
    path: '/search',
    name: 'search',
    component: SearchView
  },

  {
    path: '*',
    component: PageNotFound
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
