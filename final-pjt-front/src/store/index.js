import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '@/router/index'

Vue.use(Vuex)

const API_URL = 'http://127.0.0.1:8000'

export default new Vuex.Store({
  plugins:[
    createPersistedState(),
  ],
  state: {
    movies: [],
    movie: null,
    score: null, // detail페이지에서 현재 사용자가 리뷰한 평점을 저장하기 위한 변수
    token: null,
    comments: null,
    currentuser: null,
    recommendations: [],
    searchResult: null,
  },
  getters: {
    isLogin(state) {
      return state.token ? true : false
    }
  },
  mutations: {
    SAVE_TOKEN(state, token){
      state.token = token
      router.push({ name: 'movies'})
    },
    DELETE_TOKEN(state){
      state.token = null
      state.currentuser = null
      state.recommendations = []
      router.push({ name: 'home'})
    },
    GET_MOVIES(state, movies){
      state.movies = movies
    },
    GET_DETAIL(state, movie){
      state.movie = movie
      router.push({ name: 'movie', params: {movie_id: movie.movie_id}})
    },
    GET_SCORE(state, score) {
      state.score = score
    },
    CREATE_COMMENT(state, comment) {
      comment['is_edit'] = false
      comment['show_child'] = false
      comment['util_btn'] = false
      state.comments.unshift(comment)
    },
    DELETE_COMMENT(state, payload) {
      if (payload.is_parent) {
        state.comments.splice(payload.idx, 1)
      }
    },
    UPDATE_COMMENT(state, newComment) {
      state.comments[newComment.idx]['content'] = newComment.content
      state.comments[newComment.idx]['is_edit'] = false
    },
    GET_COMMENTS(state, comments){
      state.comments = comments.reverse()
    },
    GET_CURRENTUSER(state, currentuser) {
      state.currentuser = currentuser
    },
    GET_USER_BASED_RECOMMENDATIONS(state, data) {
      state.recommendations = data.recommendations
    },
    UPDATE_SCORE(state, payload) {
      state.movie.user.push(payload.user_id)
      state.movie.score = payload.score
    },
    SEARCH_MOVIE(state, result) {
      state.searchResult = result
    }
  },
  actions: {
    signUp(context, payload) {
      const email = payload.email
      const password1 = payload.password1
      const password2 = payload.password2
      const nickname = payload.nickname
      const sex = payload.sex
      const birthday = payload.birthday
      
      axios({
        method: 'post',
        url: `${ API_URL }/accounts/signup/`,
        data: {
          email,
          password1,
          password2, 
          nickname,
          sex,
          birthday, 
        }
      })
      .then(res => {
        context.commit('SAVE_TOKEN', res.data.key)
      })
      .catch(err=> console.log(err))
    },
    logIn(context, payload) {
      const email = payload.email
      const password = payload.password
      axios({
        method: 'post',
        url: `${ API_URL }/accounts/login/`,
        data: {
          email, 
          password,
        }
      })
      .then(res => {
        context.commit('SAVE_TOKEN', res.data.key)
      })
      .catch(err => console.log(err))
    },
    logOut(context) {
      axios({
        method: 'post',
        url: `${ API_URL }/accounts/logout/`,
      })
      .then(() => {
        context.commit('DELETE_TOKEN')
      })
      .catch(err => console.log(err))
    },
    getMovies(context) {
      axios({
        method: 'get',
        url: `${ API_URL }/api/v1/movies/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then((res) => {
        context.commit('GET_MOVIES', res.data)
      })
      .catch(err => console.log(err))
    },
    getDetail(context, payload) {
      axios({
        method: 'get',
        url: `${ API_URL }/api/v1/movies/${payload.movie_id}/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then((res) => {
        let data = res.data
        axios({
          method: 'get',
          url: `http://127.0.0.1:8000/api/v1/review/movies/${res.data.movie_id}/users/${context.state.currentuser.user_id}/`,
          headers: {
            Authorization: `Token ${ context.state.token }`
          }
        })
        .then((res) => {
          if (res.data.score) {
            data['score'] = res.data.score
          }
          else {
            data['score'] = -1
          }
          context.commit('GET_DETAIL', data)
        })
        .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
    },
    createComment(context, payload) {
      axios({
        method: 'post',
        url: `${ API_URL }/api/v1/movies/${payload.movie_id}/comments/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        },
        data: {
          content: payload.content,
        }
      })
      .then((res) => {
        res.data['profile_image'] = payload['profile_image']
        res.data['n_childs'] = payload['n_childs']
        context.commit('CREATE_COMMENT', res.data)
      })
      .catch(err => console.log(err))
    },
    deleteComment(context, payload) {
      axios({
        method: 'delete',
        url: `${ API_URL }/api/v1/comments/${payload.comment_id}/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then(() => {
        context.commit('DELETE_COMMENT', payload)
      })
      .catch(err => console.log(err))
    },
    updateComment(context, payload) {
      axios({
        method: 'put',
        url: `${ API_URL }/api/v1/comments/${payload.comment_id}/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        },
        data: {
          content: payload.newContent
        }
      })
      .then(() => {
        const newComment = {
          idx: payload.idx,
          content: payload.newContent
        }
        context.commit('UPDATE_COMMENT', newComment)
      })
      .catch(err => console.log(err))
    },
    getComments(context, payload) {
      axios({
        method: 'get',
        url: `${ API_URL }/api/v1/movies/${payload.movie_id}/comments/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then((res) => {
        for (let i=0; i<res.data.length; i++) {
          res.data[i]['is_edit'] = false
          res.data[i]['show_child'] = false
          res.data[i]['util_btn'] = false
        }
        context.commit('GET_COMMENTS', res.data)
      })
      .catch(err => {
        console.log(err)
      })
    },
    likeComment(context, payload) {
      axios({
        method: 'post',
        url: `${ API_URL }/api/v1/like/comments/${payload.comment_id}/${payload.user_id}/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then(() => {
        axios({
          method: 'get',
          url: `${ API_URL }/api/v1/movies/${payload.movie_id}/comments/`,
          headers: {
            Authorization: `Token ${ context.state.token }`
          }
        })
        .then((res) => {
          if (payload.is_parent) {
            for (let i=0; i<res.data.length; i++) {
              res.data[i]['is_edit'] = false
              res.data[i]['show_child'] = false
            }
            context.commit('GET_COMMENTS', res.data)
          }
        })
        .catch(err => {
          console.log(err)
        })
      })
      .catch(err => console.log(err))
    },
    unlikeComment(context, payload) {
      axios({
        method: 'post',
        url: `${ API_URL }/api/v1/unlike/comments/${payload.comment_id}/${payload.user_id}/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then(() => {
        axios({
          method: 'get',
          url: `${ API_URL }/api/v1/movies/${payload.movie_id}/comments/`,
          headers: {
            Authorization: `Token ${ context.state.token }`
          }
        })
        .then((res) => {
          if (payload.is_parent) {
            for (let i=0; i<res.data.length; i++) {
              res.data[i]['is_edit'] = false
              res.data[i]['show_child'] = false
            }
            context.commit('GET_COMMENTS', res.data)
          }
        })
        .catch(err => {
          console.log(err)
        })
      })
      .catch(err => console.log(err))
    },
    getCurrentUser(context) {
      axios({
        method: 'get',
        url: `${ API_URL }/api/v1/currentuser/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        }
      })
      .then((res) => {
        let data = res.data
        axios({
          method: 'get',
          url: `${ API_URL }/api/v1/profile/${ res.data.user_id }/`,
          headers: {
            Authorization: `Token ${ context.state.token }`
          }
        })
        .then((res) => {
          data['followers'] = res.data.followers
          data['followings'] = res.data.followings
          let image_file = ''
            if (res.data.profileImage_url.slice(7) === 'default.png') {
              image_file = res.data.profileImage_url.slice(7)
            }
            else {
              image_file = res.data.profileImage_url.slice(15) 
            }
          data['profileImage_url'] = `http://127.0.0.1:8000/media/profile/${image_file}/`
          context.commit('GET_CURRENTUSER', data)
          axios({
            method: 'get',
            url: `${ API_URL }/api/v1/recommendations/userbase/`,
            headers: {
              Authorization: `Token ${ context.state.token }`
            },
            params: {
              data,
            }
          })
          .then((res) => {
            context.commit('GET_USER_BASED_RECOMMENDATIONS', res.data)
          })
          .catch(err => console.log(err))
        })
        .catch(err => console.log(err))
      })
      .catch(err => console.log(err))
    },
    updateScore(context, payload) {
      axios({
        method: 'post',
        url: `${ API_URL }/api/v1/movies/${ payload.movie_id }/users/${ payload.user_id }/`,
        headers: {
          Authorization: `Token ${ context.state.token }`
        },
        data: {
          score: payload.score
        }
      })
      .then(() => {
        context.commit('UPDATE_SCORE', payload)
      })
      .catch(err => console.log(err))
    },
    searchMovie(context, payload) {
      const params = {
        'birthday': payload['birthday'],
      }
      if (!payload['keyword']) {
        let genres = ''
        for (const genre of payload['genres']) {
          genres += genre + ' '
        }
        params['genres'] = genres
      }
      else if (payload['genres'].length === 0) {
        params['keyword'] = payload['keyword']
      }
      else {
        let genres = ''
        for (const genre of payload['genres']) {
          genres += genre + ' '
        }
        params['keyword'] = payload['keyword']
        params['genres'] = genres
      }
    
      axios({
        method: 'get',
        url: `${ API_URL }/api/v1/movies/search/`,
        params: params,
        headers: {
          Authorization: `Token ${ context.state.token }`
        },
      })
      .then((res) => {
        context.commit('SEARCH_MOVIE', res.data)
      })
      .then(err => console.log(err))
    },
  },
  modules: {
  }
})
